require 'smarter_csv'

# Based on:
# https://www.nopio.com/blog/how-to-create-an-api-wrapper-of-an-external-service-in-rails/
module SpreeAPI
  module V1
    class Client
      include HttpStatusCodes
      include ApiExceptions

      def initialize(api_endpoint, api_key)
        @api_endpoint = api_endpoint
        @api_key = api_key
      end

      def show_taxonomies(id)
        response = request(
          http_method: :get,
          endpoint: "/api/v1/taxonomies/#{id}"
        )
      end

      def list_taxons(id)
        response = request(
          http_method: :get,
          endpoint: "/api/v1/taxonomies/#{id}/taxons"
        )
      end

      def create_taxons(taxonomy_id, filename)
        data = SmarterCSV.process(filename)
        data.each_with_index do |(key, value), index|
          begin
            taxon = create_taxon(taxonomy_id, { taxon: key })
            Rails.logger.info "create_taxons: successfully created taxon. index: #{index}"
          rescue StandardError => e
            Rails.logger.info "create_taxons: failed calling create_taxon. index: #{index}"
            Rails.logger.info e
          end
        end

        return true
      end
      
      def create_option_types(filename)
        data = SmarterCSV.process(filename)
        data.each_with_index do |(key, value), index|
          begin
            option_type = create_option_type({ option_type: key })
            Rails.logger.info "create_option_types: successfully created option_type. index: #{index}"
          rescue StandardError => e
            Rails.logger.info "create_option_types: failed calling create_option_type. index: #{index}"
            Rails.logger.info e
          end
        end

        return true
      end
      
      def create_option_values(option_type_id, filename)
        data = SmarterCSV.process(filename)
        data.each_with_index do |(key, value), index|
          begin
            option_type = create_option_value({ option_type_id: option_type_id, option_value: key })
            Rails.logger.info "create_option_values: successfully created option_value. index: #{index}"
          rescue StandardError => e
            Rails.logger.info "create_option_values: failed calling create_option_value. index: #{index}"
            Rails.logger.info e
          end
        end

        return true
      end
      
      def get_products(id: {})
        params = {}
        params[:ids] = id if id.present?
        response = request(
          http_method: :get,
          endpoint: "api/v1/products",
          params: params
        )
      end

      def create_products(shipping_category, image_root_path, filename)
        data = SmarterCSV.process(filename)
        prev_sku = nil
        data.each_with_index do |(key, value), index|
          next if key[:active] == 0
          
          key[:shipping_category] = shipping_category
          key[:taxon_ids] = key[:taxon_ids].split(',').map(&:to_i) if key[:taxon_ids].present?

          sku = key[:sku]
          image_filename = key[:image]

          if prev_sku == sku
            #create variant
            create_variant(index, key)
          else
             # create product
            begin
              product = create_product({ product: key })
              Rails.logger.info "create_products: successfully created product. index: #{index} #{sku}"
              actual_slug = product["slug"]
            rescue StandardError => e
              Rails.logger.info "create_products: failed calling create_product. index: #{index} #{sku}"
              Rails.logger.info e
            end

            # create image
            begin
              create_product_image(actual_slug, "#{image_root_path}/#{image_filename}")
              Rails.logger.info "create_products: successfully created product image. index: #{index} #{image_filename}"
            rescue StandardError => e
              Rails.logger.info "create_products: failed calling create_product_image. index: #{index} #{image_filename}"
              Rails.logger.info e
            end

            #create variant
            create_variant(index, key)

            prev_sku = sku
          end
        end

        return true
      end

      private

      def create_taxon(taxonomy_id, params)
        post_json("/api/v1/taxonomies/#{taxonomy_id}/taxons", params)
      end

      def create_option_type(params)
        post_json("/api/v1/option_types", params)
      end

      def create_option_value(params)
        post_json("/api/v1/option_values", params)
      end

      def create_product(params)
        post_json("/api/v1/products", params)
      end

      def create_variant(index, key)
        slug = key[:slug]
        variant_sku = key[:variant_sku]
        if variant_sku.present?
          key[:sku] = key[:variant_sku]
          key[:options] = [ { name: key[:options_name], value: key[:options_value] } ]
          key[:options].push( { name: key[:options_name2], value: key[:options_value2] } ) if key[:options_name2].present?

          [:shipping_category, :name, :short_description, :description, :meta_title, :meta_keywords, :meta_description, :tags, :taxon_ids, :options_name, :options_value, :variant_sku, :slug, :image].each { |k| key.delete(k) }
          begin
            variant = create_variant_post({ product_id: slug, variant: key })
            Rails.logger.info "create_products: successfully created variant. index: #{index} #{variant_sku}"
          rescue StandardError => e
            Rails.logger.info "create_products: failed calling create_variant. index: #{index} #{variant_sku}"
            Rails.logger.info e
          end
        end
      end

      def create_variant_post(params)
        post_json("/api/v1/variants", params)
      end

      def create_product_image(slug, image_path)
        params = { product_id: slug, image: { attachment: Faraday::UploadIO.new(image_path, 'image/jpeg') } }
        post_image_json("/api/v1/products/#{slug}/images", params)
      end

      # regular url_encoded client
      def client
        @_client ||= Faraday.new(@api_endpoint, request: { open_timeout: 10, timeout: 60 * 60 * 24 }) do |client|
          client.request :url_encoded
          client.adapter Faraday.default_adapter
        end
      end

      # request as url_encoded
      def request(http_method:, endpoint:, params: {})
        params[:token] = @api_key
        response = client.public_send(http_method, endpoint, params)
        parsed_response = Oj.load(response.body)

        return parsed_response if response_successful?(response)

        raise error_class(response, "Code: #{response.status}, request: #{params} response: #{response.body}")
      end

      # post as json
      def post_json(endpoint, params)
        params[:token] = @api_key
        response = client.post do |req|
          req.url endpoint
          req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
          req.body = params
        end
        parsed_response = Oj.load(response.body)

        return parsed_response if response_successful?(response)

        raise error_class(response, "Code: #{response.status}, request: #{params}, response: #{response.body}")
      end

      # multipart client
      def client_multipart
        @_client_multipart ||= Faraday.new(@api_endpoint) do |client_multipart|
          client_multipart.request :multipart
          client_multipart.adapter Faraday.default_adapter
        end
      end

      # post image and json
      def post_image_json(endpoint, params)
        params[:token] = @api_key
        response = client_multipart.post do |req|
          req.url endpoint
          req.headers['Content-Type'] = 'multipart/form-data'
          req.body = params
        end
        parsed_response = Oj.load(response.body)

        return parsed_response if response_successful?(response)

        raise error_class(response, "Code: #{response.status}, request: #{params}, response: #{response.body}")
      end

      def error_class(response, message)
        case response.status
        when HTTP_BAD_REQUEST_CODE
          BadRequestError.new(message)
        when HTTP_UNAUTHORIZED_CODE
          UnauthorizedError.new(message)
        when HTTP_FORBIDDEN_CODE
          ForbiddenError.new(message)
        when HTTP_NOT_FOUND_CODE
          NotFoundError.new(message)
        when HTTP_UNPROCESSABLE_ENTITY_CODE
          UnprocessableEntityError.new(message)
        else
          ApiError.new(message)
        end
      end

      def response_successful?(response)
        response.status == HTTP_OK_CODE || response.status == HTTP_CREATED_CODE
      end
      
=begin
      def order_json_params(order, utoken)
        products_arr = '{ '

        order.line_items.each do |item|
          product_id = item.variant.product.id
          product_url = @_spree.product_url(item.variant.product)
          product_name = item.name
          if item.variant.images.length == 0
            product_image = item.variant.product.images.first.my_cf_image_url("small")
          else
            product_image = item.variant.images.first.my_cf_image_url("small")
          end
          product_description = (item.variant.product.short_description.present? ? item.variant.product.short_description : item.variant.product.description)
          product_price = item.variant.product.price
          
          # Description with whitespaces/return line feed breaks YotPo.
          # Description is not used in YotPo. Send blank description per Emanuel@YotPo. 
          products_arr = products_arr + '"' + product_id.to_s + '": { ' +
                                        '"url": "' + product_url + '", ' + 
                                        '"name": "' + product_name + '", ' +
                                        '"image": "' + product_image + '", ' +
                                        '"description": "' + '' + '", ' +
                                        '"price": "' + product_price.to_s + '" }'
          products_arr = products_arr + ', '
        end

        substitute = ', '
        products_arr = products_arr.gsub(/[#{substitute}]+$/, '')
        products_arr = products_arr + ' }'
        
        json_params = '{ ' +
                        '"validate_data": true,' +
                        '"platform": "general",' +
                        '"utoken": "' + utoken + '",' +
                        '"email": "' + order.email + '",' +
                        '"customer_name": "' + order.name + '",' +
                        '"order_id": "' + order.number + '",' +
                        '"order_date": "' + order.completed_at.to_s + '",' +
                        '"currency_iso": "USD",' +
                        '"products": ' + products_arr +
                        ' }'

        json_params
      end
=end
    end
  end
end
